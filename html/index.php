<?php
require_once "./controllers/db.php";

if(isset($_POST["action"])){
    if($_POST["action"] == "insert"){
        if($_POST["title"] != "" && $_POST["description"] != ""){
            insert_todo_item($_POST["title"], $_POST["description"]);
        }
    }
    elseif($_POST["action"] == "delete"){
        if(isset($_POST["id"])){
            delete_todo_item($_POST["id"]);
        }
    }
    header("Location: /");
}
$items = get_all_todo_items();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300&display=swap" rel="stylesheet"> 
    <title>TODO-List</title>
</head>
<body>
    <div id="title">
        <h1>TODO-List</h1>
    </div>
    <form action="/" method="post" id="insert-form">
        <input type="text" name="title" placeholder="Title">
        <textarea name="description" placeholder="Description" cols="30" rows="10" form="insert-form"></textarea>
        <input type="hidden" name="action" value="insert">
        <input type="submit" value="Add">
    </form>
    <div>
        <?php
            while($row = $items->fetch_assoc()){
                $title = $row["title"];
                $description = $row["description"];
                $id = $row["id"]; 
                echo "<div class='todo-item'>
                    <div>
                        <h2>$title</h2>
                        <p>$description</p>
                    </div>
                    <form action=\"/\" method=\"post\" class='form-delete'>
                        <input type=\"hidden\" name=\"id\" value='$id'>
                        <input type=\"hidden\" name=\"action\" value='delete'>
                        <input type=\"submit\" value=\"&#9746;\" class='delete'>
                    </form>
                </div>";
            }
        ?>
    </div>
</body>
</html>