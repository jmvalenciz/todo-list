<?php
/*
This File has all the configuration and connection to our database
*/

// This variables have the information about where is our database
// and also access data
$host = "db";
$user = "todo-list";
$password = "todo-list";
$dbname = "todo-list";

// Create connection with the database
$conn = mysqli_connect($host, $user, $password, $dbname);

// Define all queries to our database
// We prepare it earlier to execute the same statement repeatedly with high efficiency
$insert_query = $conn->prepare("INSERT INTO todo_items(title, description) VALUES(?,?)");
$select_query = $conn->prepare("SELECT * FROM todo_items");
$delete_query = $conn->prepare("DELETE FROM todo_items WHERE id = ?");

if(!$conn){
    die("connection failed: $conn->connect_error");
}

function insert_todo_item($title, $description){
    global $insert_query;
    // we use prepare() to prevent a sql injection     
    $insert_query->bind_param('ss', $title, $description);
    $insert_query->execute();
    print_r($insert_query->get_result());
}

function get_all_todo_items(){
    global $select_query;
    $select_query->execute();
    return $select_query->get_result();
}

function delete_todo_item($id){
    global $delete_query;
    $delete_query->bind_param('i', $id);
    $delete_query->execute();
}

?>