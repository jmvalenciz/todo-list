# Introduction

In this tutorial we will learn how to build a `TODO List` web page using
this tools:

-   `html`
-   `css`
-   `javascript`
-   `php`
-   `phpmyadmin`
-   `apache`
-   `mariadb`

The idea of this web page is to be able to perform `CRUD` operations on
a list of elements of things to do.

# Requirements of the system 

To be sure that our web page is correctly built, we will use the next
list of requirements to check each thing that we need to do:

-   I as an user, should be able to insert a new task to the todo list
-   I as an user, should be able to list all the tasks that I've
    inserted in the todo list
-   I as an user, should be able to delete any task on the todo list

Thus, at the end of this tutorial, each element ot this list will be
completed

# Preparation 

First of all, for this tutorial we will use a `GNU/Linux` OS so we will
show the commands for the main distributions. If
you are using Windows, we recommend you to use the [Windows Subsystem
for Linux](https://docs.microsoft.com/en-us/windows/wsl/install-win10) and then, you will have to use the instructions for
Ubuntu or you can use [Docker Desktop for Windows](https://docs.docker.com/docker-for-windows/install/). For this project, we
will create the folder `~/projects/todo-list` when we will save our code
and configuration. You can use `mkdir -p ~projects/todo-list` to create
the folder and inside, we have to have the next folders:
![][5]

After that, we have to install the necessary tools in our computers.

# Docker

We will use a container environment to avoid damaging our computers and
because it's easier to configure this environment that our own machines
because depending on the distro we are using, the configuration may
vary. So, for install docker, we will use the next commands:

For **Arch Linux** and **Manjaro**:

~~~ bash
sudo pacman -S docker
~~~

For **Ubuntu** artful:

~~~ bash
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu artful stable"
sudo apt update
apt-cache policy docker-ce
sudo apt install docker-ce
sudo systemctl status docker
~~~

For **MacOS**:

~~~ bash
brew install docker docker-machine
brew cask install virtualbox
docker-machine create --driver virtualbox default
~~~

For **Amazon Linux 2**:

~~~ bash
sudo yum update -y
sudo amazon-linux-extras install docker
sudo service docker start
~~~

For **Fedora** and **Centos**:

~~~ bash
sudo yum install -y yum-utils device-mapper-persistent-data lvm2
sudo yum-config-manager  --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install docker-ce
sudo systemctl enable docker
sudo systemctl start docker
~~~

For **Debian** 10:

~~~ bash
sudo apt update
sudo apt install apt-transport-https ca-certificates curl gnupg2 software-properties-common
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
sudo apt update
apt-cache policy docker-ce
sudo apt install docker-ce
sudo systemctl enable docker
sudo systemctl start docker
~~~

And this instrucction is for all linux distros to run docker as normal
user:

~~~ bash
sudo usermod -a -G docker $(whoami)
~~~

## Installing the rest of the tools

Once docker is installed, our environment will be ready and the next
commands and instructions will be the same for all the OS. Now, we have
to install and configure all the tools we mentioned at the beginning of
this tutorial and remember: if you can't execute any of the next
commands, maybe you need to use `sudo` or a root user instead.

### HTML, CSS & JavaScript {#html-css-javascript}

This is the easiest part of the tutorial because for this you just need
any web browser as `Google Chrome`, `Firefox`, `Edge` or `Safari` so we
will not spend time on this step and we will continue.

### Apache, MariaDB, PHP & PhpMyAdmin {#apache-mariadb-php-phpmyadmin}

We will install this four tools over `docker` and for that, we will
follow the next steps:

1.  Create a network to have the same web for all our containers or in
    other words:

    ~~~ bash
    docker network create todo-list-network
    ~~~

    Now, we have to put `--network todo-list-network` on each
    `docker run` command to add our containers to ths network

2.  Here we will start MariaDB container but you can use MySQL and we
    will show you what you have to change to do this. Also, we will set
    the next environment variables:

    -   `MYSQL_ROOT_PASSWORD` with our root password

    -   `MYSQL_DATABASE` with the name of our database

    -   `MYSQL_USER` with the name of our user

    -   `MYSQL_PASSWORD` with the password for our user We will set a
        volume to persist our data out of the container binding a folder
        of our system with a folder in the container

        ~~~ bash
        docker run --name db \
            --network todo-list-network \
            --volume ~/projects/todo-list/mysql:/var/lib/mysql \
            -e MYSQL_USER=todo-list \
            -e MYSQL_PASSWORD=todo-list \
            -e MYSQL_ROOT_PASSWORD=toor \
            -e MYSQL_DATABASE=todo-list \
            -d mariadb:10.5 # Here you can change mariadb:10.5 to mysql:8.0 if you want to use MySQL
        ~~~
3.  First, we will configure an image with a configuration for mysql and
    mariadb. You have to make a folder in your project with the name
    `php-apache` and inside, a file called `Dockerfile` and you have to
    put this configuration inside that file:

    ~~~ dockerfile
    FROM php:7.4-apache
    RUN docker-php-ext-install mysqli
    RUN a2enmod rewrite
    RUN service apache2 restart
    EXPOSE 80
    ~~~

    After that, you have to execute the next command in your console and
    remember that you have to be in the same folder that your `Dockerfile`:
    ~~~ sh
    sudo docker build -t todo-list/php-apache .
    ~~~
    
    Now we will start Apache with PHP preconfigured and we will pass our
    project folder as an argument to bind this folder with a folder
    inside the container where apache searches for files

    ~~~ bash
    docker run --name php-apache \
        -d -p 80:80 \
        --network todo-list-network \
        --volume ~/projects/todo-list/html:/var/www/html \
        todo-list/php-apache
    ~~~

4.  Start PhpMyAdmin indicating the name of our database container

    ~~~ bash
    docker run --name phpmyadmin \
       -d -p 81:80 \
       -e PMA_HOST=db \
       --network todo-list-network \
       phpmyadmin/phpmyadmin:latest
    ~~~

    As we can see, we added environment variables to MariaDB for
    `root password` to specify the password of the root user and
    `database` to specify the name of the database that we will use and
    we passed the name of our container instead of the IP because docker
    will replace this domain name with the ip of our database and this
    is very useful because docker will assign a different ip to our
    container each time it runs it.

Now we should be able to see our containers running if we use the
command:

~~~ bash
docker ps -a
~~~

At the end of this, we will have access to our containers on the next
ways:

-   **To PhpMyAdmin:** from your computer using `localhost:81` or from
    containers in `todo-list-network` using `phpmyadmin:81`
-   **To Apache:** from your computer using `http://localhost` or from
    containers in `todo-list-network` using `http://php-apache`
-   **To MariaDB:** just from containers in `todo-list-network` using
    `db:3306`

# Checking Environment 

## Checking if PHP is Working 

At this point, we have an environment for php working on a apache server
and for mariadb or MySQL depending on what you choosed. Now, we will
check if php is working correctly making a new file called `index.php`
in our project folder and we will put the next code inside it:

~~~ php
<?php
phpinfo();
?>
~~~

And we will see something like this when we access to
`http://localhost`: 
![][1] 
With that, you can be sure that php is
working correctly.

Checking if MySQL and PhpMyAdmin are Working 

This step is really easy: we have to go to `localhost:81` and login with
our user: `todo-list` and password: `todo-list` and if the login is
successful, everything is correct.

# Configuring Database

We will save our todo-list in our database so we have to set it up to be
able to save our data. First of all, we have to login with our user and
password and we will see this: 
![][2] 
And we have to choose our
`todo-list` database indicated by the red arrow on the image and after
that, choose the `SQL` tab: 
![][3] 
And we will put the next instruction
inside the text field:

~~~ sql
CREATE TABLE todo_items(
    CREATE TABLE todo_items(
    id INT NOT NULL AUTO_INCREMENT,
    title VARCHAR(20),
    description VARCHAR(100),
    PRIMARY KEY (id)
);
~~~

Press the `Go` button and you should see something like this: 
![][4]
Now, our database is ready to store our data.

# Programming our web application

One everything is ready, we can start coding and we will start with the
first functionality: insert new elements.

## Controller for the database

We will create a new file called `db.php` that will have the next code:

~~~ php
<?php
/*
This File has all the configuration and connection to our database
*/

// This variables have the information about where is our database
// and also access data
$host = "db";
$user = "todo-list";
$password = "todo-list";
$dbname = "todo-list";

// Create connection with the database
$conn = mysqli_connect($host, $user, $password, $dbname);

// Define all queries to our database
// We prepare it earlier to execute the same statement repeatedly with high efficiency
$insert_query = $conn->prepare("INSERT INTO todo_items(title, description) VALUES(?,?)");
$select_query = $conn->prepare("SELECT * FROM todo_items");
$delete_query = $conn->prepare("DELETE FROM todo_items WHERE id = ?");

if(!$conn){
    die("connection failed: $conn->connect_error");
}

function insert_todo_item($title, $description){
    global $insert_query;
    // we use prepare() to prevent a sql injection
    $insert_query->bind_param('ss', $title, $description);
    $insert_query->execute();
    print_r($insert_query->get_result());
}

function get_all_todo_items(){
    global $select_query;
    $select_query->execute();
    return $select_query->get_result();
}

function delete_todo_item($id){
    global $delete_query;
    $delete_query->bind_param('i', $id);
    $delete_query->execute();
}

?>
~~~

This file has all the configuration about the connection to our database
and functions that can insert, list and delete todo-items stored in our
database.

## Main page

Now, we will create an `index.php` file inside our `html` folder and we
will divide the explanation into two sections but remember: all code in
this section is part of `index.php`.

### Controller

This little code here manages all the requests from the view. It checks
if the view sends something using the `POST` method and more
specifically, it checks if the view sends a variable called `action`
using `POST` and this variable could be `"insert"` to add a new item to
the database or `"delete"` to remove an item.

~~~ php
<?php
require_once "./controllers/db.php";

if(isset($_POST["action"])){
    if($_POST["action"] == "insert"){
        if($_POST["title"] != "" && $_POST["description"] != ""){
            insert_todo_item($_POST["title"], $_POST["description"]);
        }
    }
    elseif($_POST["action"] == "delete"){
        if(isset($_POST["id"])){
            delete_todo_item($_POST["id"]);
        }
    }
    header("Location: /");
}
$items = get_all_todo_items();
?>
~~~

### View

This section has two well distinguishable sections:

-   **insert-form** that receives a title and a description and send a
    `POST` request with a variable called `action` setted as `insert`
-   **todo-items** that has a list of items with title, description and
    a delete button. This delete button sends a `POST` request with a
    variable called `action` setted as `delete` Also, this section has a
    reference to a file called `styles.css` and to a font from Google
    Fonts

~~~ html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300&display=swap" rel="stylesheet">
    <title>TODO-List</title>
</head>
<body>
    <div id="title">
        <h1>TODO-List</h1>
    </div>
    <form action="/" method="post" id="insert-form">
        <input type="text" name="title" placeholder="Title">
        <textarea name="description" placeholder="Description" cols="30" rows="10" form="insert-form"></textarea>
        <input type="hidden" name="action" value="insert">
        <input type="submit" value="Add">
    </form>
    <div>
        <?php
            while($row = $items->fetch_assoc()){
                $title = $row["title"];
                $description = $row["description"];
                $id = $row["id"];
                echo "<div class='todo-item'>
                    <div>
                        <h2>$title</h2>
                        <p>$description</p>
                    </div>
                    <form action=\"/\" method=\"post\" class='form-delete'>
                        <input type=\"hidden\" name=\"id\" value='$id'>
                        <input type=\"hidden\" name=\"action\" value='delete'>
                        <input type=\"submit\" value=\"&#9746;\" class='delete'>
                    </form>
                </div>";
            }
        ?>
    </div>
</body>
</html>
~~~

## Styles

Here, we have a file called `styles.css` that has all the styles for
`index.php`. We will save this file in the folder `http`.

~~~ css
body{
    background-color: #F5F5F5;
    font-family: 'Ubuntu', sans-serif;
}

#title{
    text-align: center;
    font-size: 26px;
}

.form-delete{
    margin-left: auto;
}

.delete{
    border:none;
    background-color: none;
    font-size: xx-large;
    margin: 1rem;
    color: red;
    border-radius: 8px;
}

.todo-item{
    background-color: #d1d1d1;
    margin-left: 10rem;
    margin-right: 10rem;
    margin-top: 2rem;
    display: flex;
    flex-direction: row;
    border-radius: 1rem;
}

.todo-item > div {
    margin-left: 5rem;
}

#insert-form{
    display: flex;
    flex-direction: column;
    margin-left: 10rem;
    margin-right: 10rem;
}

#insert-form > *{
    margin-top: 10px;
}
~~~

  [1]: ./IMG/phpinfo.png
  [2]: ./IMG/phpmyadmin_login.png
  [3]: ./IMG/phpmyadmin_database_sql.png
  [4]: ./IMG/database_created.png
  [5]: ./IMG/folder_structure.png